# uso autorizado desde que cite a fonte.
# coding:utf8

import platform
import os

class Puzzle:

    tab = []

    def __init__(self, tab=[1, 2, 3, 4, 0, 5, 6, 7, 8]):
        self.tab = tab

    # função para limpar a tela.
    def limpar(self):
        sis_op = platform.system()
        if sis_op == "Windows":
            os.system("cls")
        else:
            os.system("clear")


    # Verifica se o espaço pode ser movido para a esquerda
    def temVizinhoEsquerda(self):
        return self.tab.index(0) % 3 > 0


    # Verifica se o espaço pode ser movido para a direita
    def temVizinhoDireita(self):
        return self.tab.index(0) % 3 < 2


    # Verifica se o espaço pode ser movido para cima
    def temVizinhoAcima(self):
        return self.tab.index(0) > 2


    # Verifica se o espaço pode ser movido para baixo
    def temVizinhoAbaixo(self):
        return self.tab.index(0) < 6


    # executa o movimento
    def mover(self, mov):
        posZero = self.tab.index(0)
        if mov.lower() == "e" and self.temVizinhoEsquerda():
            self.tab[posZero - 1], self.tab[posZero] = self.tab[posZero], self.tab[posZero - 1]
        elif mov.lower() == "d" and self.temVizinhoDireita():
            self.tab[posZero + 1], self.tab[posZero] = self.tab[posZero], self.tab[posZero + 1]
        elif mov.lower() == "c" and self.temVizinhoAcima():
            self.tab[posZero - 3], self.tab[posZero] = self.tab[posZero], self.tab[posZero - 3]
        elif mov.lower() == "b" and self.temVizinhoAbaixo():
            self.tab[posZero + 3], self.tab[posZero] = self.tab[posZero], self.tab[posZero + 3]
        else:
            print "Movimento inválido"
        return


    def mostra(self):
        print "Tabuleiro "
        k = 0
        for i in range(3):
            for j in range(3):
                print self.tab[k],
                k += 1
            print