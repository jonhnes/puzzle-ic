# encoding:utf8
# qpy:console
#
# programa ilustrando o uso de grafos
#
#
from Model import Vertex


class Graph:
    def __init__(self):
        self.numVerticesMaximo = 20
        self.numVertices = 0
        self.listaVertices = []
        self.matrizAdjacencias = []
        for i in range(self.numVerticesMaximo):
            linhaMatriz = []
            for j in range(self.numVerticesMaximo):
                linhaMatriz.append(0)
            self.matrizAdjacencias.append(linhaMatriz)

    def adicionaVertice(self, rotulo):
        self.numVertices += 1
        self.listaVertices.append(Vertex(rotulo))

    def adicionaArco(self, inicio, fim):
        self.matrizAdjacencias[inicio][fim] = 1
        self.matrizAdjacencias[fim][inicio] = 1


    def mostraVertice(self, vertice):
        print self.listaVertices[vertice].rotulo

    def imprimeMatriz(self):
        print " ",
        for i in range(self.numVertices):
            print self.listaVertices[i].rotulo,
        print
        for i in range(self.numVertices):
            print self.listaVertices[i].rotulo,
            for j in range(self.numVertices):
                print self.matrizAdjacencias[i][j],
            print

    def localizaRotulo(self, rotulo):
        for i in range(self.numVertices):
            if self.listaVertices[i].igualA(rotulo): return i
        return -1

    def obtemAdjacenteNaoVisitado(self, v):
        for i in range(self.numVertices):
            if self.matrizAdjacencias[v][i] == 1 and self.listaVertices[i].foiVisitado() == False:
                return i
        return -1

    def dfs(self, inicio, fim):
        pilha = []
        self.listaVertices[inicio].regVisitado()
        pilha.append(inicio)  # usado como push
        while len(pilha) != 0:
            elementoAnalisar = pilha[len(pilha) - 1]
            if elementoAnalisar == fim:
                print "Meta Encontrada, o Caminho é: ",
                for i in pilha: print self.listaVertices[i].rotulo,
                print
                break
            v = self.obtemAdjacenteNaoVisitado(elementoAnalisar)
            if v == -1:
                pilha.pop()
            else:
                self.listaVertices[v].regVisitado()
                pilha.append(v)
        else:
            print "Caminho não encontrado. Busca sem sucesso "
        for i in self.listaVertices:
            i.limpa()