from Model import Puzzle


puzzle = Puzzle()
while True:
    puzzle.limpar()
    puzzle.mostra()
    movimento = raw_input("Qual movimento ((E)esquerda,(D)direita,(C)cima,(B)baixo (S) Sair )? ")
    if movimento.lower() == "s":
        break
    else:
        puzzle.mover(movimento)